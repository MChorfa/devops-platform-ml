FROM python:3.7-slim-buster
COPY ./mlapp.py /app/mlapp.py 
COPY  ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
RUN python -m spacy download en_core_web_md
ENTRYPOINT [ "streamlit", "run" ]
CMD [ "mlapp.py" ]
